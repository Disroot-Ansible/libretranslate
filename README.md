# Libretranslate - Ansible Role

This role covers deployment, configuration and software updates of Libretranslate. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.

`vagrant up`

`ansible-playbook -b Playbooks/libretranslate.yml`

Then you can access Roundcube from your computer on http://192.168.33.56


## Playbook
The playbook includes nginx role and deploys entire stack needed to run Libretranslate. Additional role is also available in the Ansible roles repos in git.

## Server configuration
You should have at least:
 - 3GB RAM
 - 25GB disk size

On the first run, the role downloads all packages, so be aware that it can be a bit long as this is about 8GB to be downloaded!

Then, when you first lauch libretranslate, it downloads all languages packages (around 8GB in `.local/share/argos-translate/packages`)


## Updating
You just need to update the `libretranslate_version` var and to run the role to update.


## CHANGELOG
- **05.12.2023** - Add possibility to choose version and to update models
- **08.10.2023** - Create the role
